package com.example.ps.myapplication;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.Random;

/**
 * Activity that holds bulk of the logic for counting steps.
 * There are two views supported by this activity: Debug and Customer view.
 * You can switch between the two views by tapping 5 times on the screen while the app is open.
 * The step counter has been tested while holding the phone in hand.
 */
public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mTypeStepDetector;
    private Sensor mTypeStepCounter;

    private LineChart chart;

    private static final int DATA_SIZE = 100;
    private double[] accelerationData = new double[DATA_SIZE];
    private int dataIndex = 0;
    private static final float SMOOTHING_CONSTANT = 0.25f;

    private int peakCount = 0;
    private double peakSum = 0;
    private int stepCount = 0;

    private int androidStepDetectorCount = 0;

    private float androidStepCounterLastReading = 0f;
    private int androidStepCounterCount = 0;

    private boolean isDebugViewVisible = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeChart();
        initializeSensors();
        setupToggleDebugViewTouchListener();
    }

    private void initializeChart() {
        chart = findViewById(R.id.chart);
        chart.setTouchEnabled(false);
        chart.setDragEnabled(false);
        chart.setScaleEnabled(false);
        chart.setDrawGridBackground(false);
        chart.setPinchZoom(false);
        chart.setBackgroundColor(Color.WHITE);
        chart.setData(new LineData());
        chart.getDescription().setText(getString(R.string.debug_chart_label));
        chart.invalidate();
    }

    private void initializeSensors() {
        final PackageManager packageManager = getApplicationContext().getPackageManager();
        this.mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_ACCELEROMETER)) {
            this.mAccelerometer = this.mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        } else {
            final TextView textView = findViewById(R.id.rawAccelerationTextView);
            textView.setText(getString(R.string.debug_android_step_accelerometer_not_found));
            textView.invalidate();
        }
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_DETECTOR)) {
            this.mTypeStepDetector = this.mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        } else {
            final TextView textView = findViewById(R.id.androidStepDetectorTextView);
            textView.setText(getString(R.string.debug_android_step_detector_not_found));
            textView.invalidate();
        }
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER)) {
            this.mTypeStepCounter = this.mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        } else {
            final TextView textView = findViewById(R.id.androidStepCounterTextView);
            textView.setText(getString(R.string.debug_android_step_counter_not_found));
            textView.invalidate();
        }
    }

    private void setupToggleDebugViewTouchListener() {
        final View mainView = findViewById(R.id.mainView);
        mainView.setOnTouchListener(new View.OnTouchListener() {
            private int touches = 0;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                touches += 1;
                if (touches % 5 == 0) {
                    toggleDebugView();
                }
                v.performClick();
                return false;
            }
        });
    }

    private void toggleDebugView() {
        final ViewGroup transitionContainer = findViewById(R.id.debugView);
        TransitionManager.beginDelayedTransition(transitionContainer);
        isDebugViewVisible = !isDebugViewVisible;
        transitionContainer.setVisibility(isDebugViewVisible ? View.VISIBLE : View.GONE);
        transitionContainer.invalidate();
        final ViewGroup customerViewGroup = findViewById(R.id.customerView);
        TransitionManager.beginDelayedTransition(customerViewGroup);
        final TextView newStepTextView = findViewById(R.id.newStepView);
        newStepTextView.setVisibility(!isDebugViewVisible ? View.VISIBLE : View.GONE);
        newStepTextView.invalidate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAccelerometer != null) {
            mSensorManager.registerListener(this,
                    mAccelerometer,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
        if (mTypeStepDetector != null) {
            mSensorManager.registerListener(this,
                    mTypeStepDetector,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
        if (mTypeStepCounter != null) {
            mSensorManager.registerListener(this,
                    mTypeStepCounter,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    private void addEntry(float yValue) {
        final LineData data = chart.getData();
        final ILineDataSet set = data.getDataSetByIndex(0);
        if (set == null) {
            data.addDataSet(createSet(getString(R.string.debug_raw_data_set), Color.RED));
            data.addDataSet(createSet(getString(R.string.debug_smooth_data_set), Color.GREEN));
        }

        data.addEntry(new Entry(data.getDataSetByIndex(0).getEntryCount(), yValue), 0);
        chart.notifyDataSetChanged();
        chart.setVisibleXRangeMaximum(DATA_SIZE);
        chart.moveViewTo(data.getEntryCount() - 7, 50f, YAxis.AxisDependency.LEFT);

        int previousIndex = dataIndex == 0 ? DATA_SIZE - 1 : dataIndex - 1;
        accelerationData[dataIndex] = accelerationData[previousIndex] +
                SMOOTHING_CONSTANT * (yValue - accelerationData[previousIndex]);
        data.addEntry(new Entry(data.getDataSetByIndex(1).getEntryCount(),
                (float)accelerationData[dataIndex]), 1);

        double forwardSlope =
                accelerationData[previousIndex] -
                        accelerationData[previousIndex == 0 ? DATA_SIZE - 1 : previousIndex - 1];
        double backwardSlope = accelerationData[previousIndex] - accelerationData[dataIndex];
        if (forwardSlope > 0 && backwardSlope > 0 && accelerationData[dataIndex] > 0.8) {
            this.peakCount += 1;
            this.peakSum += accelerationData[previousIndex];
        }
        double peakMean = peakCount == 0 ? 0 : peakSum / peakCount;

        updateDebugMetrics(peakMean);

        if (forwardSlope > 0 && backwardSlope > 0
                && accelerationData[previousIndex] > 0.8 * peakMean
                && accelerationData[previousIndex] > 1) {
            this.stepCount += 1;
            updateCustomerStepCount();
            updateDebugStepCount();
        }
        dataIndex = (dataIndex + 1)  % DATA_SIZE;
    }

    private void updateCustomerStepCount() {
        final TextView newStepTextView = findViewById(R.id.newStepView);
        final String text = this.stepCount == 1 ? " step" : " steps";
        newStepTextView.setText(
                String.format(getString(R.string.customer_steps), this.stepCount, text)
        );
        final Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256),
                rnd.nextInt(256));
        newStepTextView.setTextColor(color);
        newStepTextView.invalidate();
    }

    private void updateDebugMetrics(double peakMean) {
        final TextView peakCountTextView = findViewById(R.id.peakCountTextView);
        peakCountTextView.setText(String.format(getString(R.string.debug_peak_count),
                this.peakCount));
        peakCountTextView.invalidate();

        final TextView peakSumTextView = findViewById(R.id.peakSumTextView);
        peakSumTextView.setText(String.format(getString(R.string.debug_peak_sum), this.peakSum));
        peakSumTextView.invalidate();

        final TextView peakMeanTextView = findViewById(R.id.peakMeanTextView);
        peakMeanTextView.setText(String.format(getString(R.string.debug_peak_mean), peakMean));
        peakMeanTextView.invalidate();
    }

    private void updateDebugStepCount() {
        final TextView stepCountTextView = findViewById(R.id.stepCountTextView);
        stepCountTextView.setText(String.format(getString(R.string.debug_step_count),
                this.stepCount));
        stepCountTextView.invalidate();
    }

    private LineDataSet createSet(final String dataSetName, final int color) {
        final LineDataSet set = new LineDataSet(null, dataSetName);
        set.setLineWidth(1f);
        set.setCircleRadius(1f);
        set.setColor(color);
        set.setCircleColor(color);
        set.setHighLightColor(color);
        set.setValueTextColor(color);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setValueTextSize(10f);
        return set;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            double magnitude = Math.sqrt(Math.pow(event.values[0], 2) +
                    Math.pow(event.values[1], 2) +
                    Math.pow(event.values[2], 2));
            addEntry((float)magnitude);
            final TextView textView = findViewById(R.id.rawAccelerationTextView);
            textView.setText(String.format(getString(R.string.debug_raw_acceleration), magnitude));
            textView.invalidate();
        } else if (event.sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
            this.androidStepDetectorCount += 1;
            final TextView textView = findViewById(R.id.androidStepDetectorTextView);
            textView.setText(String.format(getString(R.string.debug_android_step_detector),
                    androidStepDetectorCount));
            textView.invalidate();
        } else if (event.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
            if (this.androidStepCounterLastReading == 0f) {
                this.androidStepCounterLastReading = event.values[0];
            }
            this.androidStepCounterCount += event.values[0] - this.androidStepCounterLastReading;
            this.androidStepCounterLastReading = event.values[0];
            final TextView textView = findViewById(R.id.androidStepCounterTextView);
            textView.setText(String.format(getString(R.string.debug_android_step_counter),
                    androidStepCounterCount));
            textView.invalidate();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}
